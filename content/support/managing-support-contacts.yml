---
title: Managing Support contacts and handling details
description: Taking control over who has access to your support entitlement
support-hero:
  data:  
    title: GitLab Support
    content: |
      The GitLab support team is here to help. To avoid any delays in processing your tickets we recommend seeding a list of named contacts. If a ticket is submitted by someone not on your list, we'll ask them to <a href="/support/managing-support-contacts/#proving-your-support-entitlement">prove their support entitlement</a> which will delay our initial response.
side_menu:
  anchors:
    text: 'ON THIS PAGE'
    data:
      - text: 'Getting Set Up'
        href: '#getting-set-up'
        nodes:
          - text: 'Proving your Support Entitlement'
            href: '#proving-your-support-entitlement'
          - text: 'For GitLab.com Users'
            href: '#for-gitlab-com-users'
          - text: 'For Self-managed Users'
            href: '#for-self-managed-users'                                   
      - text: "Managing contacts"
        href: "#managing-contacts"
        nodes:
          - text: "Maximum number of support contacts"
            href: "#maximum-number-of-contacts"
          - text: "Using an email alias or distribution group as a support contact"
            href: "#using-an-email-alias-or-distribution-group-as-a-support-contact"
          - text: "Authorized contacts"
            href: "#authorized-contacts"
      - text: "Shared Organizations"
        href: "#shared-organizations"
      - text: "Special Handling Notes"
        href: "#special-handling-notes"                                          
  hyperlinks:
    text: ''
    data: []
components:
  - name: support-copy-markdown
    data:
      block:
        - header: Getting Set Up
          id: getting-set-up
          text: |
            Once your license or subscription is provisioned, we recommend submitting an initial ticket with a list of contacts who are allowed to contact Support.
            
            1. Submit a Support ticket using the Support portal related matters form and select First time setup in the Problem type field.
            2. You'll likely receive a reply letting you know that you need to prove your support entitlement, so be prepared with those details.
        - subtitle:
            id: proving-your-support-entitlement
            text: Proving your Support Entitlement
          text: |
            Depending on how you purchased GitLab, GitLab Support may not automatically detect your support entitlement on the creation of your first support ticket. If that's the case, you will be asked to provide evidence that you have an active license or subscription.
        - subtitle:
            id: for-gitlab-com-users
            text: For GitLab.com Users
          text: |
            To ensure that we can match you with your GitLab.com subscription when opening a support ticket, please:
            - include your GitLab.com username; AND
            - use the primary email address associated with your account; AND
            - reference a path within a GitLab.com group (that you are a member of or attempting to join), which has a valid subscription associated with it (such as a link to a problematic pipeline or MR)
        - subtitle:
            id: for-self-managed-users
            text: For Self-managed Users
          text:  |
            To ensure that we can match you with your Self-managed license when opening a support ticket, please:
            - use a company provided email address (no generic email addresses such as Gmail, Yahoo, etc.); AND
            - provide GitLab with one of the following:
              - A screenshot of the license page
                - versions older than 14.1 see `/admin/license`
                - versions 14.1 or newer see `/admin/subscription`
              - The output of the command `sudo gitlab-rake gitlab:license:info`
              - The license ID displayed on the /admin/license page (GitLab 13.2+)
              - The license file provided to you at the time of purchase **and at least one of these:**
                - the email address to which the license was issued
                - the company name to which the license was issued
        - header: Managing contacts
          id: managing-contacts
          text: |
              Additional Support contacts can be managed by any user who can [prove their support entitlement](/support/managing-support-contacts/#proving-your-support-entitlement) by submitting a request using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and selecting `Manage my organization's contacts` in the Problem type field.
        - subtitle:
            text: Maximum number of support contacts
            id: maximum-number-of-contacts
          text: |
            Currently, we limit the maximum number of support contacts to 30 per organization.
            Should a request to add more or setup a shared organization arise when at the limit (or when the request would put you over the limit), the Support Operations team will discuss this with you to find a resolution.
        - subtitle:
            text: Using an email alias or distribution group as a support contact
            id: using-an-email-alias-or-distribution-group-as-a-support-contact
          text: |
            Some organizations prefer to use a generic email address like an alias or distribution group for one of their registered support contacts. This will work, but for the smoothest experience consider the following:

            1. Set a login password for this support user and share it within your team.
            2. When you raise a ticket, always log in: this will allow you to add CCs to any tickets you raise.
            3. CC any email addresses that may be involved in the resolution of the ticket: this will allow other individuals in the organization to reply to the ticket via email.
        - subtitle:
            text: Authorized contacts
            id: authorized-contacts
          text: |
            For organizations that require additional security, you can specify a set of authorized contacts who can make changes.
            1. Submit a Support ticket using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select Other requests in the Problem type field.
            
            We will add an internal note detailing who is allowed to make changes to the contacts in your organization.
        - header: Shared Organizations
          id: shared-organizations
          text: |
            In some cases, certain organizations want all members of their organization to be able to see all of the support tickets that have been logged. In other cases, a particular user from the account would like to be able to see and respond to all tickets from their organization. If you'd like to enable this, please:
            **Global Support Shared Organization Setup**
            1. Submit a Support ticket using the Global [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `Shared organization requests` in the Problem type field.
            
            **US Federal Support Shared Organization Setup**
            1. Submit a Support ticket in the US Federal Support portal's [requests for shared organization setup form](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421052) and fill in what type of sharing you would like setup. Kindly note that if you wish to enable shared organizations in both portals you must submit a separate request using the Global Support portal form.
        - header: Special Handling Notes
          id: special-handling-notes
          text: |
            If there's a special set of handling instructions you'd like included in your Organizations notes, we'll do our best to comply. Not all requests can be accommodated, but if there is something we can do to make your support experience better we want to know.

            1. Submit a Support ticket using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `Other Requests` in the Problem type field.
